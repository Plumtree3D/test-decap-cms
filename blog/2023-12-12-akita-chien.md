---
layout: blog
title: Akita (chien) v2
date: 2023-12-12T08:03:08.460Z
thumbnail: images/high-school-et-un-jock-nerd-bjdcwa.jpg
rating: -4
---
import HomepageFeatures from '@site/src/components/HomepageFeatures';

![](https://fr.wikipedia.org/wiki/Akita_(chien)#/media/Fichier:Akita_Inu_breed_plemeno.jpg)

L’akita, également appelé akita inu et akita ken (秋田犬?, littéralement « chien d’Akita »), est une race de chien originaire du Japon.

Il ne doit pas être confondu avec son cousin le Shiba Inu, qui lui ressemble en apparence mais dont la taille et le poids diffèrent grandement. 

[wikipedia.org/wiki/Akita_(chien)](https://fr.wikipedia.org/wiki/Akita_(chien))

<iframe width="560" height="315" src="https://www.youtube.com/embed/VqoyKzgkqR4?si=MElVraO9uHNxe27K" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<HomepageFeatures/>